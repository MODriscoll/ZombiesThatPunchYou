// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "ZombieJumpNavLinkComponent.generated.h"

/**
 * 
 */
UCLASS()
class CHARACTERTESTING_API AZombieJumpNavLink : public AActor
{
	GENERATED_BODY()
	
public:

	AZombieJumpNavLink();

	virtual void OnConstruction(const FTransform& Transform) override;

	UPROPERTY()
	class UNavLinkComponent* NavLink;
	
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
	class USphereComponent* LeftCollider;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
	class USphereComponent* RightCollider;

	UPROPERTY(EditInstanceOnly, BlueprintReadWrite, meta = (MakeEditWidget = "true"))
	FVector Left;

	UPROPERTY(EditInstanceOnly, BlueprintReadWrite, meta = (MakeEditWidget = "true"))
	FVector Right;
};
