// Fill out your copyright notice in the Description page of Project Settings.

#include "GameVersionFunctionLibrary.h"

bool UGameVersionFunctionLibrary::IsWithEditor()
{
	// WITH_EDITOR is either 0 or 1 (false or true)
	return static_cast<bool>(WITH_EDITOR);
}

bool UGameVersionFunctionLibrary::IsShippingBuild()
{
	// UE_BUILD_SHIPPING is either 0 or 1 (false or true)
	return static_cast<bool>(UE_BUILD_SHIPPING);
}


