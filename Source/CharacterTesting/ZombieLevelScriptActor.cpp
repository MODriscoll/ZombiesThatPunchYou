// Fill out your copyright notice in the Description page of Project Settings.

#include "ZombieLevelScriptActor.h"

#include "Kismet/GameplayStatics.h"
#include "Blueprint/AIBlueprintHelperLibrary.h"

TArray<APawn*> AZombieLevelScriptActor::SetZombieSpawn(int32 Amount, TSubclassOf<APawn> Template, FName TagToFind) const
{
	TArray<APawn*> zombies;

	// First make sure there are actors with matching tag
	TArray<AActor*> SpawnPoints; 
	UGameplayStatics::GetAllActorsWithTag(GetWorld(), TagToFind, SpawnPoints);

	if (SpawnPoints.Num() > 0)
	{
		for (int32 i = 0; i < Amount; ++i)
		{		
			int32 random = FMath::RandRange(0, SpawnPoints.Num() - 1);
			FTransform transform = SpawnPoints[random]->GetActorTransform();

			zombies.Push(UAIBlueprintHelperLibrary::SpawnAIFromClass(GetWorld(), Template, nullptr,
				transform.GetLocation(), transform.GetRotation().Rotator(), true));
		}
	}

	return zombies;
}


