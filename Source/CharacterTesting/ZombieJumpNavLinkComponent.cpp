// Fill out your copyright notice in the Description page of Project Settings.

#include "ZombieJumpNavLinkComponent.h"

#include "AI/Navigation/NavLinkComponent.h"
#include "AI/Navigation/NavigationSystem.h"
#include "Components/SphereComponent.h"

AZombieJumpNavLink::AZombieJumpNavLink()
{
	PrimaryActorTick.bCanEverTick = false;

	Left = FVector(5.f, 0.f, 0.f);
	Right = FVector(-5.f, 0.f, 0.f);

	NavLink = CreateDefaultSubobject<UNavLinkComponent>(TEXT("NavLink"));
	RootComponent = NavLink;

	LeftCollider = CreateDefaultSubobject<USphereComponent>(TEXT("Left Collider"));
	LeftCollider->SetupAttachment(NavLink);

	LeftCollider->SetSphereRadius(100.f, false);
	LeftCollider->SetRelativeLocation(Left);

	RightCollider = CreateDefaultSubobject<USphereComponent>(TEXT("Right Collider"));
	RightCollider->SetupAttachment(NavLink);

	RightCollider->SetSphereRadius(100.f, false);
	RightCollider->SetRelativeLocation(Right);
}

void AZombieJumpNavLink::OnConstruction(const FTransform& Transform)
{
	if (!NavLink->Links.IsValidIndex(0))
	{
		NavLink->Links.AddDefaulted();

	}

	FNavigationLink& link = NavLink->Links[0];
	link.Left = Left;
	link.Right = Right;
	link.Direction = ENavLinkDirection::LeftToRight;

	LeftCollider->SetRelativeLocation(Left);
	RightCollider->SetRelativeLocation(Right);

	UWorld* World = GetWorld();
	if (World)
	{
		UNavigationSystem* NavSystem = World->GetNavigationSystem();
		if (NavSystem)
		{
			NavSystem->UpdateComponentInNavOctree(*NavLink);
		}
	}
}


