// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AIController.h"
#include "ZombieController.generated.h"

/**
 * 
 */
UCLASS()
class CHARACTERTESTING_API AZombieController : public AAIController
{
	GENERATED_BODY()	

public:

	AZombieController(const FObjectInitializer& PCIP);
};
