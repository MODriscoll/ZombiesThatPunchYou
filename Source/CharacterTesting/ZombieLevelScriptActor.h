// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/LevelScriptActor.h"
#include "ZombieLevelScriptActor.generated.h"

/**
 * 
 */
UCLASS()
class CHARACTERTESTING_API AZombieLevelScriptActor : public ALevelScriptActor
{
	GENERATED_BODY()
	
public:

	UFUNCTION(BlueprintCallable, Category = "Spawning")
	TArray<APawn*> SetZombieSpawn(int32 Amount, TSubclassOf<APawn> Template, FName TagToFind = TEXT("ZombieSpawn")) const;
};
