// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "GameVersionFunctionLibrary.generated.h"

/**
 * 
 */
UCLASS()
class CHARACTERTESTING_API UGameVersionFunctionLibrary : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()

	// Returns if the build of the game is with the editor
	UFUNCTION(BlueprintPure, Category = "Game Build")
	static bool IsWithEditor();

	// Returns if the build of the game is a shipping build
	UFUNCTION(BlueprintPure, Category = "Game Build")
	static bool IsShippingBuild();
};
